// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })
    .state('app.card', {
      url: "/card",
      views: {
        'menuContent' :{
          templateUrl: "templates/card.html",
          controller: 'CardCtrl'
        }
      }
    })
    .state('app.credits', {
      url: "/credits",
      views: {
        'menuContent' :{
          templateUrl: "templates/credits.html"
        }
      }
    })

    .state('app.notices', {
      url: "/notices",
      views: {
        'menuContent' :{
          templateUrl: "templates/notices.html"
        }
      }
    })    
    .state('app.info', {
      url: "/info",
      views: {
        'menuContent' :{
          templateUrl: "templates/info.html",
          controller: 'InfoCtrl'
        }
      }
    })
    .state('app.about', {
      url: "/info/about",
      views: {
        'menuContent' :{
          templateUrl: "templates/info/about.html",
        }
      }
    })
    .state('app.collecting', {
      url: "/info/collecting",
      views: {
        'menuContent' :{
          templateUrl: "templates/info/collecting.html",
        }
      }
    })
    .state('app.spending', {
      url: "/info/spending",
      views: {
        'menuContent' :{
          templateUrl: "templates/info/spending.html",
        }
      }
    })
    .state('app.faq', {
      url: "/info/faq",
      views: {
        'menuContent' :{
          templateUrl: "templates/info/faq.html",
          controller: 'FaqCtrl'
        }
      }
    })
    .state('app.faqAnswer', {
      url: "/info/faq/:faqId",
      views: {
        'menuContent' :{
          templateUrl: "templates/info/faq_answer.html",
          controller: 'FaqAnswerCtrl'
        }
      }
    })    
    .state('app.news', {
      url: "/info/news",
      views: {
        'menuContent' :{
          templateUrl: "templates/info/news.html",
        }
      }
    })
    .state('app.contact', {
      url: "/info/contact",
      views: {
        'menuContent' :{
          templateUrl: "templates/info/contact.html",
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/card');
});

