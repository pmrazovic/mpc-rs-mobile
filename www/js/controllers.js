angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope) {
})

.controller('CardCtrl', function($scope) {

  var doOnOrientationChange = function () {
    switch(window.orientation)
    {  
      case -90:
      case 90:
        $("#cardPreview").hide();
        $("#barcode").barcode("01100030278292", "code128", {barWidth: 4, barHeight: 200, fontSize: 20});
        $("#barcode").show();
        break; 
      default:
        $("#cardPreview").show();
        $("#barcode").hide();
        break; 
    }
  }

  $("#barcodeSmall").barcode("01100030278292", "code128", {barWidth: 2, barHeight: 60, fontSize: 12});
  doOnOrientationChange();
  window.addEventListener('orientationchange', doOnOrientationChange);
})

.controller('InfoCtrl', function($scope) {
})

.controller('FaqCtrl', function($scope, $http) {
  $scope.questions = [];
  $http.get("js/faq.json", {})
    .success(function (data) {
      $scope.questions = data;
  });
})

.controller('FaqAnswerCtrl', function($scope, $http, $stateParams, $filter) {
  $http.get("js/faq.json", {})
    .success(function (data) {
      $scope.questions = data;
      $scope.question = $filter('filter')($scope.questions, {id: $stateParams.faqId})[0];
  });  
})